from tictactoe import GameBoard


def test_check_user_input1():
    board = GameBoard(["1", "2", "3", "4", "5", "6", "7", "8", "9"])
    board.cell = 3
    board.check_user_input(3)
    assert board.cell == 3


def test_check_user_input2():
    board = GameBoard(["1", "2", "3", "4", "5", "6", "7", "8", "9"])
    board.cell = 6
    board.check_user_input(6)
    assert board.cell == 6


def test_check_user_input3():
    board = GameBoard(["1", "2", "3", "4", "5", "6", "7", "8", "9"])
    board.cell = 8
    board.check_user_input(8)
    assert board.cell == 8
