from tictactoe import GameBoard


def test_check1():
    board = GameBoard(["X", "X", "X", "4", "5", "6", "7", "8", "9"])
    assert board.check() is True


def test_check2():
    board = GameBoard(["1", "2", "O", "4", "O", "6", "O", "8", "9"])
    assert board.check() is True


def test_check3():
    board = GameBoard(["1", "2", "O", "4", "5", "O", "7", "8", "9"])
    assert board.check() is False
