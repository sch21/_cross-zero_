from tictactoe import GameBoard


def test1_isXO():
    board = GameBoard(["1", "2", "3", "4", "5", "6", "7", "8", "9"])
    assert board.is_XO(0) is False


def test2_isXO():
    board = GameBoard(["1", "2", "3", "4", "5", "6", "7", "8", "9"])
    assert board.is_XO(5) is False


def test3_isXO():
    board = GameBoard(["X", "2", "3", "4", "5", "6", "7", "8", "9"])
    assert board.is_XO(0) is True


def test4_isXO():
    board = GameBoard(["O", "2", "3", "4", "5", "6", "7", "8", "9"])
    assert board.is_XO(0) is True
