from tictactoe import GameBoard


def test1_change_player():
    board = GameBoard(["1", "2", "3", "4", "5", "6", "7", "8", "9"])
    board.player_number = -1
    board.change_player()
    assert board.player_number == 1


def test2_change_player():
    board = GameBoard(["1", "2", "3", "4", "5", "6", "7", "8", "9"])
    board.player_number = 1
    board.change_player()
    assert board.player_number == -1


def test3_change_player():
    board = GameBoard(["1", "2", "3", "4", "5", "6", "7", "8", "9"])
    board.player_number = -1
    board.change_player()
    board.change_player()
    assert board.player_number == -1
