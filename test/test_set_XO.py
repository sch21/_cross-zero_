from tictactoe import GameBoard


def test_setXO1():
    board = GameBoard(["1", "2", "3", "4", "5", "6", "7", "8", "9"])
    board.player_number = 1
    board.set_XO(0)
    assert board.places == ["X", "2", "3", "4", "5", "6", "7", "8", "9"]


def test_setXO2():
    board = GameBoard(["1", "2", "3", "4", "5", "6", "7", "8", "9"])
    board.player_number = -1
    board.set_XO(1)
    assert board.places == ["1", "O", "3", "4", "5", "6", "7", "8", "9"]


def test_setXO3():
    board = GameBoard(["1", "2", "3", "4", "5", "6", "7", "8", "9"])
    board.player_number = 1
    board.set_XO(1)
    assert board.places == ["1", "X", "3", "4", "5", "6", "7", "8", "9"]
