from custom_error import TooLargeError, TooSmallError


class GameBoard:
    def __init__(self, places):
        self.places = places
        self.player_number = 1

    def board_to_screen(self):
        print("    ", self.places[0], self.places[1], self.places[2])
        print("    ", self.places[3], self.places[4], self.places[5])
        print("    ", self.places[6], self.places[7], self.places[8])

    def player_move(self):
        if self.player_number > 0:
            print("Ход Х-игрока")
        else:
            print("Ход O-игрока")

    def where(self) -> int:
        while True:
            try:
                cell = int(input("Выберите 1-9 для хода, 0 для выхода:\n-> "))
                self.check_user_input(cell)
            except (ValueError, TooSmallError, TooLargeError) as e:
                print(e)
                self.board_to_screen()

            else:
                break
        return cell

    def check(self) -> bool:
        if((self.places[0] == self.places[1] == self.places[2]) or
           (self.places[3] == self.places[4] == self.places[5]) or
           (self.places[6] == self.places[7] == self.places[8]) or
           (self.places[0] == self.places[3] == self.places[6]) or
           (self.places[1] == self.places[4] == self.places[7]) or
           (self.places[2] == self.places[5] == self.places[8]) or
           (self.places[0] == self.places[4] == self.places[8]) or
           (self.places[2] == self.places[4] == self.places[6])):
            return True
        else:
            return False

    def check_user_input(self, cell: int) -> int:
        if cell < 0:
            raise TooSmallError
        elif cell > 9:
            raise TooLargeError
        return cell

    def set_XO(self, cell: int) -> None:
        if self.player_number > 0:
            self.places[cell] = "X"
        else:
            self.places[cell] = "O"

    def is_XO(self, cell: int) -> bool:
        return (self.places[cell] == "X" or self.places[cell] == "O")

    def change_player(self) -> None:
        self.player_number *= -1


def main():
    board = GameBoard(["1", "2", "3", "4", "5", "6", "7", "8", "9"])
    cell = 1
    print(" КРЕСТИКИ-НОЛИКИ")
    for i in range(10):
        board.board_to_screen()
        board.player_move()
        cell = board.where()
        if not cell:
            break
        if 0 < cell < 10:
            cell -= 1
            if not board.is_XO(cell):
                board.set_XO(cell)
                board.change_player()
                if board.check():
                    break
    board.board_to_screen()
    print("Game over")


if __name__ == "__main__":
    main()
