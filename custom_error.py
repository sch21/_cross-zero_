class TooLargeError(Exception):
    def __str__(self) -> str:
        return "Слишком большое число \n"


class TooSmallError(Exception):
    def __str__(self) -> str:
        return "Слишком маленькое число \n"
